# Matlab_Dynamic_Programming

MATLAB Code: Solve Fibonacci Numbers using Dynamic Programming, Memoization

Implementation in MATLAB


Fibo1.m 
Fibonacci with Recursive approach:
    Time Complexity:    O(2^n)
    Space Complexity:  	O(2^n)
	
Fibo2.m 
Fibonacci with Dynamic programming (Memoization):
    Time Complexity:    O(n)
    Space Complexity:   O(n)

Fibo3.m 
Fibonacci with Matrix Exponentiation:
	Time Complexity:    O(log(n))
