
% Using Recursive Method
tic; Fibo1(40); toc

% Using Dynamic Programming
tic; Fibo2(40); toc

%%

% Using Dynamic Programming
tic; Fibo2(10000); toc

% Using Matrix Exponentiation
tic; Fibo3(10000); toc

